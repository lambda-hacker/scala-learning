// Tail recursive implementation of Higher order functions for lists

object hof {
  def map[A, B] (f: A => B)(l: List[A]) : List[B] = {
      def map_aux(l: List[A], acc: List[B]) : List[B] = l match {
        case Nil => acc
        case h::t => map_aux(t, f(h) :: acc)
    }
    map_aux(l, Nil).reverse
  }

  def flatMap[A, B] (f: A => List[B]) (l: List[A]) : List[B] = {
    def flmap_aux(xs: List[A], acc: List[B]) : List[B] = {
      xs match {
        case Nil => acc
        case h::t => flmap_aux(t, acc ++ f(h))
      }
    }
    flmap_aux(l, Nil)
  }

  def filter[A] (p: A => Boolean) (l: List[A]) : List[A] = {
    def filter_aux(l: List[A], acc: List[A]) : List[A] = l match {
      case Nil => acc
      case h::t => if (p(h)) filter_aux(t, h::acc) else filter_aux(t, acc)}
    filter_aux(l, Nil).reverse
  }

  def zip[T, S] (xs: List[T], ys: List[S]) : List[(T, S)] = {
    def zip_aux(l1 : List[T], l2: List[S], acc: List[(T, S)]) : List[(T, S)] =
      (l1, l2) match {
        case (Nil, _) | (_, Nil) => acc
        case (h1::t1, h2::t2) => zip_aux( t1, t2, (h1, h2) :: acc )
      }
    zip_aux(xs, ys, Nil).reverse
  }


  def zipWith[T, S] (f: (T, S) => Any) (xs: List[T], ys: List[S]) : List[Any] = {
    val r = zip(xs, ys)
    map ({ (t:(T, S)) => f(t._1, t._2) }) (r)
  }

  def zipWithIndex[S , T] (xs: List[S], init: Int = 0) : List[(S, Int)] = {
    def zipWI_aux (l : List[S], i: Int, acc: List[(S, Int)]) : List[(S, Int)] = {
      l match {
        case Nil => acc.reverse
        case h::t => zipWI_aux(t, i + 1, (h, i) :: acc)
      }
    }
    zipWI_aux(xs, init, Nil)

  }

  def foldLeft[A, B] (base: B)(f: (B, A) => B) (l: List[A]) : B = {
    l match {
      case Nil => base
      case h::t  =>  foldLeft (f(base, h)) (f) (t)
    }
  }

  def take[A] (n: Int) (l: List[A]) : List[A]  = {
    def take_aux(v: Int, xs: List[A], acc: List[A]) : List[A] =
      (v, xs) match {
        case (0, _) | (_, Nil) => acc
        case (v, h::t) => take_aux(v - 1, t, h::acc)
      }
    if (n < 0) Nil
    else take_aux(n, l, Nil).reverse
  }

  def takeWhile[A] (pred: A => Boolean) (l: List[A]) : List[A]  = {
    def takewl_aux(xs: List[A], acc: List[A]) : List[A] =
      xs match {
        case Nil => acc
        case h::t => if ( pred(h) ) takewl_aux(t, h::acc) else acc
      }
    takewl_aux(l, Nil).reverse
  }

  def drop[A] (n: Int) (l: List[A]) : List[A]  = {
    def drop_aux(v: Int, xs: List[A]) : List[A] =
      (v, xs) match {
        case (0, _) | (_, Nil) => xs
        case (v, h::t) => drop_aux(v - 1, t)
      }
    if (n <= 0) l
    else drop_aux(n, l)
  }

  def dropWhile[A] (pred: A => Boolean) (l: List[A]) : List[A]  = {
    def dropwl_aux(xs: List[A]) : List[A] =
      xs match {
        case Nil => Nil
        case h::t => if ( pred(h) ) dropwl_aux(t) else xs
      }
    dropwl_aux (l)
  }

  def exists[A] (v: A) (l: List[A]) : Boolean = l match {
    case Nil => false
    case h::t => if (v == h) true else exists(v)(t)
  }

  def main(args: Array[String])= {
    val a = List(1, "Hi", 2.3);
    println (a)

    val b = (1 to 25).toList
    val r = filter ((v:Int) => v % 2 == 0) (map ((x:Int) => x * x) (b))
    println (r)

    val t = take (10) (r)
    println (t)
    println (take (20) (r))
    println (take (-1) (r))
  }
}
