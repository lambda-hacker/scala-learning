package lambda.hacker.funcs

object Learn_Sheet {
	// Test
	println("Welcome to the Scala worksheet") //> Welcome to the Scala worksheet
	println("Hello Hackers!")                 //> Hello Hackers!

	// 99 Problems

	// P1:

	def last[A](l : List[A]) : A = {
		l match {
			case Nil => throw new NoSuchElementException
			case x :: Nil => x
			case _ :: t => last(t)
		}
	}                                         //> last: [A](l: List[A])A

	last(List("Hello", 3, 4.0, 'M'))          //> res0: Any = M
	last(List(3, 2, 4, 5, 0, -2, -7))         //> res1: Int = -7

	// P2:

	def penultimate[A](l : List[A]) : A = {
		l match {

			case x :: Nil => throw new NoSuchElementException
			case x :: _ :: Nil => x
			case _ :: t => penultimate(t)
		}
	}                                         //> penultimate: [A](l: List[A])A

	penultimate(List(1, 5, 3, 7, 9))          //> res2: Int = 7
	penultimate(List("Hello", "Hi"))          //> res3: String = Hello
	penultimate(List(3.1415, 'V', "Last"))    //> res4: Any = V

	// P3:

	def nth[A](i : Int, l : List[A]) : A = {
		(l, i) match {
			case (Nil, _) => throw new NoSuchElementException
			case (h :: t, 0) => h
			case (h :: t, _) => nth(i - 1, t)
		}
	}                                         //> nth: [A](i: Int, l: List[A])A

	nth(3, List(1, 5, 3, 7, 9))               //> res5: Int = 7
	nth(1, List("Hello", "Hi"))               //> res6: String = Hi
	nth(2, List(3.1415, 'V', "Last"))         //> res7: Any = Last

	// P4:

	def length[A](l : List[A]) : Int = {
		def len(c : Int, l: List[A]) : Int = {
			l match {
				case Nil => c
				case _ :: t => len(c + 1, t)
			}
		}
		len(0, l)
	}                                         //> length: [A](l: List[A])Int
	
	length(List("Twenty", 3.15, 'V', 29))     //> res8: Int = 4

	// P5:

	def reverse[A](l : List[A]) : List[A] = {
		l match {
			case Nil => Nil
			case h :: t => reverse(t) ::: List(h)
		}

	}                                         //> reverse: [A](l: List[A])List[A]

	def rev_trec[A](l : List[A]) : List[A] = {
		def revtr[A](rev_l : List[A], l : List[A]) : List[A] = {
			(l, rev_l) match {
				case (Nil, _) => rev_l
				case (h :: t, _) => revtr(h :: rev_l, t)
			}
		}
		revtr(Nil, l)
	}                                         //> rev_trec: [A](l: List[A])List[A]

	rev_trec(List(1, 2, 3, 4, 5))             //> res9: List[Int] = List(5, 4, 3, 2, 1)

	rev_trec(List("Hello", 3.14, 'M', -5))    //> res10: List[Any] = List(-5, M, 3.14, Hello)

	// P6:
	def isPalindrome[A](l : List[A]) : Boolean = {
		l == rev_trec(l)
	}                                         //> isPalindrome: [A](l: List[A])Boolean

	isPalindrome("5423245".toList)            //> res11: Boolean = true
	isPalindrome(List(1, 2, 3, 2, 1))         //> res12: Boolean = true

	// P7: Flatten TODO: for 'Any' type

	def flatten[A](l : List[A]) : List[A] = {
		l match {
			case Nil => Nil
			case (e : List[A]) :: t => flatten(e) ++ flatten(t)
			case (e : A) :: t => List(e) ++ flatten(t)
		}
	}                                         //> flatten: [A](l: List[A])List[A]
	flatten(List(List(1, 1), 2, List(3, List(5, 8))))
                                                  //> res13: List[Any] = List(1, 1, 2, 3, 5, 8)

	// P8:
	def compress[A](l : List[A]) : List[A] = {
		def compr[A] (l: List[A], acc: List[A]) : List[A] = {
			l match {
			
				case Nil => acc
				case x :: y :: Nil => if (x == y) compr(Nil, x::Nil) else compr(Nil, x::y::acc)
				case x :: y :: t => if (x == y) compr(y :: t, acc ) else  compr(y :: t, x :: acc)
				case _ => l
			}
		}
	  compr(l, Nil)
	}                                         //> compress: [A](l: List[A])List[A]

	compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
                                                  //> res14: List[Symbol] = List('e)

	compress(List('x, 'y, 'y))                //> res15: List[Symbol] = List('y)

	// P9:

	def pack[A](l : List[A]) : List[List[A]] = {

		l match {
			case Nil => Nil
			case h :: t => List(h :: t.takeWhile(_ == h)) ++ pack(t.dropWhile(_ == h))
		}
	}                                         //> pack: [A](l: List[A])List[List[A]]
	pack(List('x, 'y, 'y))                    //> res16: List[List[Symbol]] = List(List('x), List('y, 'y))
	pack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
                                                  //> res17: List[List[Symbol]] = List(List('a, 'a, 'a, 'a), List('b), List('c, '
                                                  //| c), List('a, 'a), List('d), List('e, 'e, 'e, 'e))

	// P10:

	def encode[A](l : List[A]) : List[(Int, A)] = {
		pack(l).map((l) => (l.length, l.head))
	}                                         //> encode: [A](l: List[A])List[(Int, A)]

	encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
                                                  //> res18: List[(Int, Symbol)] = List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (
                                                  //| 4,'e))

	// P11:

	def encodeModified[A](l : List[A]) : List[Any] = {
		pack(l).map((xs) => if (xs.length == 1) xs.head else (xs.length, xs.head))
	}                                         //> encodeModified: [A](l: List[A])List[Any]

	encodeModified(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
                                                  //> res19: List[Any] = List((4,'a), 'b, (2,'c), (2,'a), 'd, (4,'e))

	// P12:
	def decode[A](l : List[(Int, A)]) : List[A] = {
		l match {
			case Nil => Nil
			case (i, e) :: t => List.fill(i)(e) ++ decode(t)
		}
	}                                         //> decode: [A](l: List[(Int, A)])List[A]

	decode(List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e)))
                                                  //> res20: List[Symbol] = List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 
                                                  //| 'e, 'e)

	// P13:
	def encodeDirect[A](l : List[A]) : List[(Int, A)] = {
		def enc[A](l : List[A], count : Int) : List[(Int, A)] = {
			l match {
				case Nil => Nil
				case a :: Nil => (count + 1, a) :: Nil
				case a :: b :: t => if (a == b) enc(b :: t, count + 1) else (count + 1, a) :: enc(b :: t, 0)
			}

		}
		enc(l, 0)
	}                                         //> encodeDirect: [A](l: List[A])List[(Int, A)]

	encodeDirect(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
                                                  //> res21: List[(Int, Symbol)] = List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (
                                                  //| 4,'e))
	encodeDirect(List('x, 'x, 'z))            //> res22: List[(Int, Symbol)] = List((2,'x), (1,'z))
	encodeDirect(List())                      //> res23: List[(Int, Nothing)] = List()
	(4, 'a) :: Nil                            //> res24: List[(Int, Symbol)] = List((4,'a))

	//P14:

	def duplicate[A](l : List[A]) : List[A] = {
		l match {
			case Nil => Nil
			case h :: t => h :: h :: duplicate(t)
		}
	}                                         //> duplicate: [A](l: List[A])List[A]

	duplicate(List('a, 'b, 'c, 'c, 'd))       //> res25: List[Symbol] = List('a, 'a, 'b, 'b, 'c, 'c, 'c, 'c, 'd, 'd)

	//P15:
	def duplicateN[A](n : Int, l : List[A]) : List[A] = {
		def duplicate_n(n : Int, c : Int, l : List[A]) : List[A] = {
			l match {
				case Nil => Nil
				case h :: t => if (n == c) duplicate_n(n, 0, t) else h :: duplicate_n(n, c + 1, l)
			}
		}
		duplicate_n(n, 0, l)

	}                                         //> duplicateN: [A](n: Int, l: List[A])List[A]

	duplicateN(3, List('a, 'b, 'c, 'c, 'd))   //> res26: List[Symbol] = List('a, 'a, 'a, 'b, 'b, 'b, 'c, 'c, 'c, 'c, 'c, 'c, 
                                                  //| 'd, 'd, 'd)
	duplicateN(4, "Testing".toList)           //> res27: List[Char] = List(T, T, T, T, e, e, e, e, s, s, s, s, t, t, t, t, i,
                                                  //|  i, i, i, n, n, n, n, g, g, g, g)

	//P16:

	def drop[A](nth : Int, l : List[A]) : List[A] = {
		def dp[A](nth : Int, c : Int, l : List[A]) : List[A] = {
			l match {
				case Nil => Nil
				case h :: t => if (c == nth) dp(nth, 1, t) else h :: dp(nth, c + 1, t)
			}
		}
		dp(nth, 1, l)
	}                                         //> drop: [A](nth: Int, l: List[A])List[A]

	drop(4, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
                                                  //> res28: List[Symbol] = List('a, 'b, 'c, 'e, 'f, 'g, 'i, 'j, 'k)

	//P17:
	def split[A](k : Int, l : List[A]) : (List[A], List[A]) = {
		def spl[A](i : Int, k : Int, acc : List[A], l : List[A]) : (List[A], List[A]) = {
			(i == k, l) match {
				case (false, h :: t) => spl(i + 1, k, acc ::: List(h), t)
				case (true, r) => (acc, r)
			}
		}
		spl(0, k, Nil, l)
	}                                         //> split: [A](k: Int, l: List[A])(List[A], List[A])
	split(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
                                                  //> res29: (List[Symbol], List[Symbol]) = (List('a, 'b, 'c),List('d, 'e, 'f, 'g
                                                  //| , 'h, 'i, 'j, 'k))
	1                                         //> res30: Int(1) = 1

	//P18: Slice  slice(3, 7, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
	def slice[A](a : Int, b : Int, l : List[A]) : List[A] = {
		def slc[A](p : Int, a : Int, b : Int, l : List[A]) : List[A] = {
			(a < b, l) match {
				case (false, _) | (_, Nil) => Nil
				case (true, h :: t) => if (p < a) slc(p + 1, a, b, t) else h :: slc(p + 1, a + 1, b, t)
			}
		}
		slc(0, a, b, l)
	}                                         //> slice: [A](a: Int, b: Int, l: List[A])List[A]

	slice(3, 7, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
                                                  //> res31: List[Symbol] = List('d, 'e, 'f, 'g)

	//P19:
	def rotate[A](sh : Int, l : List[A]) : List[A] = {
		def rot[A](i : Int, k : Int, acc : List[A], l : List[A]) : List[A] = {
			(i == k, l) match {
				case (false, h :: t) => rot(i + 1, k, acc ::: List(h), t)
				case (true, r) => r ++ acc
			}
		}
		if (sh < 0) rot(0, l.length + sh, Nil, l)
		else rot(0, sh, Nil, l)
	}                                         //> rotate: [A](sh: Int, l: List[A])List[A]
	rotate(-2, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
                                                  //> res32: List[Symbol] = List('j, 'k, 'a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i)

	//P20:
	def removeAt[A](k : Int, l : List[A]) : List[A] = {
		def remk[A](i : Int, k : Int, l : List[A]) : List[A] = {
			l match {
				case Nil => Nil
				case h :: t => if (i == k) t else h :: remk(i + 1, k, t)
			}
		}
		remk(0, k, l)
	}                                         //> removeAt: [A](k: Int, l: List[A])List[A]

	removeAt(1, List('a, 'b, 'c, 'd))         //> res33: List[Symbol] = List('a, 'c, 'd)

	//P21:
	def insert[A](v : A, nth : Int, l : List[A]) : List[A] = {
		def in[A](v : A, i : Int, k : Int, acc : List[A], l : List[A]) : List[A] = {
			(i < k, l) match {
				case (_, Nil) => Nil
				case (false, _) => acc ++ List(v) ++ l
				case (true, h :: t) => in(v, i + 1, k, acc ::: List(h), t)
			}
		}
		in(v, 0, nth, Nil, l)

	}                                         //> insert: [A](v: A, nth: Int, l: List[A])List[A]

	insert('new, 1, List('a, 'b, 'c, 'd))     //> res34: List[Symbol] = List('a, 'new, 'b, 'c, 'd)

	//P22
	def range(a : Int, b : Int) : List[Int] = {
		(a <= b) match {
			case true => a :: range(a + 1, b)
			case false => Nil
		}
	}                                         //> range: (a: Int, b: Int)List[Int]

	range(4, 9)                               //> res35: List[Int] = List(4, 5, 6, 7, 8, 9)

	//P23

	// Pascal Triangle
	def pascal_triangle(k : Int) : List[List[Int]] = {
		def ncr(n : Int, r : Int) : Int = {
			(n - r + 1 to n).product / (1 to r).product
		}
		def pascal_line(i : Int) : List[Int] = {
			((0 to i) map ((v) => ncr(i, v))).toList
		}
		((0 to k - 1) map ((i) => pascal_line(i))).toList
	}                                         //> pascal_triangle: (k: Int)List[List[Int]]

	pascal_triangle(10) map ({ (l) => l map ((e) => print(e + " ")); println })
                                                  //> 1 
                                                  //| 1 1 
                                                  //| 1 2 1 
                                                  //| 1 3 3 1 
                                                  //| 1 4 6 4 1 
                                                  //| 1 5 10 10 5 1 
                                                  //| 1 6 15 20 15 6 1 
                                                  //| 1 7 21 35 35 21 7 1 
                                                  //| 1 8 28 56 70 56 28 8 1 
                                                  //| 1 9 36 84 126 126 84 36 9 1 
                                                  //| res36: List[Unit] = List((), (), (), (), (), (), (), (), (), ())
}
	 