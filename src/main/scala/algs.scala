// Write it for generic with proper variance, currently only for List[Int]

 object algs {
  def merge [A] (a: Seq[A], b: Seq[A], comp: (A, A) => Boolean) : Seq[A] = {
    (a, b) match {
        case (Nil, _) => b
        case (_, Nil) => a
        case (x::xs, y::ys) => if (comp(x,y))  x +: merge(xs, b, comp) else y +: merge(a, ys, comp)
    }
  }


  def msort [A] (l : Seq[A], comp: (A, A) => Boolean) : Seq[A] = {

    l match {
      case Nil => Nil
      case x::Nil => l
      case _ => {
        val mid = l.length / 2
        val (left, right) = l.splitAt(mid)
        merge(msort(left, comp), msort(right, comp), comp)

      }
    }
  }


  def binSearch [A] (v: A, equal: (A, A) => Boolean, less: (A, A) => Boolean) (l : Seq[A]) : Option[Int] = {
    def bin_aux (low: Int, high: Int) : Int = {
      if (low > high) return -1
      val mid:Int  = (high + low) / 2

      val isEquals = equal(v, l(mid))
      val isLess = less(v, l(mid))
      val isGreater = !isEquals && !isLess

      (isEquals, isLess, isGreater) match {
        case (true,_,_) => mid
        case (_, true, _) => bin_aux (low, mid - 1)
        case (_, _, true) => bin_aux (mid + 1, high)
      }
    }

    val r = bin_aux (0, l.length-1)
    if (r != -1) Some (r)
    else None
  }


  def main(args: Array[String]): Unit  = {
    println ("Hi Hackers!")
  }
}
